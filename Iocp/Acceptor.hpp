#ifndef _ACCEPTOR_HPP_
#define _ACCEPTOR_HPP_
#include "ExternalSocket.hpp"
#include <string>

class Acceptor
	:
	public AbstractSocket
{
private:
	Base& InternalBase;
	SOCKET Connection;
	HANDLE CompletionPort;
	ADDRINFO* Result;
	const std::string& PortInternal;
	LPFN_ACCEPTEX AcceptExInternal;
	LPFN_GETACCEPTEXSOCKADDRS GetAcceptExSockAddrsInternal;
public:
	Acceptor
	(
		Base& InternalBase,
		const std::string& Port
	);
	~Acceptor();
	SOCKET GetSocket();
	bool Accept
	(
		ExternalSocket* Socket,
		PVOID Extra,
		Iocp::AcceptExCompletionRoutine Completion
	);
	
private:
	void CreateAddressInformationWithPassive(ADDRINFO* Information);
	bool CreateSocket
	(
		const std::string& Port,
		ADDRINFO* Information
	);
	bool Listen();
	
protected:
	

	bool CreateServerSocket(const std::string& Port);
	
};
#endif