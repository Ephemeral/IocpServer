#ifndef _BASE_HPP_
#define _BASE_HPP_
#include "OverlappedExtended.hpp"
#include <MSWSock.h>



class Base 
	:
	public Abstract
{
private:

	struct ThreadBlock
	{
		HANDLE ThreadHandle;
		uint64_t ThreadId;
	};

	ThreadBlock CompletionThreads[8];
	
	WSADATA Wsa;

	HANDLE CompletionPort;

public:
	Base();
	Abstract* This();
	LPFN_ACCEPTEX InitializeAcceptEx(SOCKET Socket);
	LPFN_GETACCEPTEXSOCKADDRS InitializeGetacceptExSockAddrs(SOCKET Socket);
	LPFN_CONNECTEX InitializeConnectEx(SOCKET Socket);
	HANDLE AssociateCompletionPort
	(
		HANDLE Port, 
		PVOID Key
	);
	~Base();
private:
	

	bool CreateCompletionThreads();

	
	HANDLE AssociateCompletionPort
	(
		HANDLE Port, 
		HANDLE ExistingPort, 
		PVOID Key
	);
	static void WsaCompletionRoutine
	(
		ULONG32 Error,
		ULONG32 Transferred,
		OverlappedExtended* Extended
	);
	
	static uint32_t CALLBACK CompletionThread(PVOID Context);

	HANDLE GetCompletionPort();

	bool StartWsa();

	void AbstractHandleAcceptInternal(OverlappedExtended* Overlapped);
};


#endif