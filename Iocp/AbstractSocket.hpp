#ifndef _ABSTRACTSOCKET_HPP_
#define _ABSTRACTSOCKET_HPP_
#include "AbstractOverlapped.hpp"
class AbstractSocket;

class AbstractSocket
{
private:
public:
	virtual SOCKET	GetSocket			()											= 0;
	virtual bool	ReadWriteInternal	(AbstractOverlapped* OverlappedExtended)	= 0;
private:
};

#endif