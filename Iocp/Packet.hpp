#ifndef _PACKET_HPP_
#define _PACKET_HPP_
#include "AbstractSocket.hpp"
class Io
{
private:
	uint32_t Offset;
	uint32_t Size;
	WSABUF WsaBuffer;
public:
	
	uint8_t* Raw;
	
	
		
	Io(uint32_t Size)
	{
		Init(Size);
		Raw = new uint8_t[Size];
		RtlSecureZeroMemory(Raw, Size);
		Fix();
	}
	/*
		uint32_t Size: Size to write
		uint8_t* Raw: Buffer to write 
		Note:
		An internal buffer is allocated within the Io class and memory is copied from the argument
		The external uint8_t* Raw is no longer needed beyond this point
	*/
	Io
	(
		uint32_t Size,
		uint8_t* Raw
	)
	{
		Init(Size);
		this->Raw = new uint8_t[Size];
		memcpy(this->Raw, Raw, Size);
		Fix();
	}
	void UpdateOffset(uint32_t Transferred)
	{
		Offset += Transferred;
		WsaBuffer.buf = (char*)(Raw + Offset);
		WsaBuffer.len = Size - Offset;
	}
	bool IsComplete()
	{
		return Offset == Size;
	}
	uint32_t GetTotalSizeOfAllocation()
	{
		return Size;
	}
	uint32_t GetSizeOfCompletion()
	{
		return Offset;
	}
	PVOID Reserved()
	{
		return &WsaBuffer;
	}
	~Io()
	{
		if (Raw != NULL)
		{
			RtlSecureZeroMemory(Raw, Size);
			delete[] Raw;
			Raw = NULL;
		}
		
		RtlSecureZeroMemory
		(
			&WsaBuffer, 
			sizeof(WSABUF)
		);
	}
private:
	void Init(uint32_t Size)
	{
		Offset = 0;
		RtlSecureZeroMemory(&WsaBuffer, sizeof(WSABUF));
		this->Size = Size;
	}
	void Fix()
	{
		WsaBuffer.buf = (char*)Raw;
		WsaBuffer.len = Size;
	}
};

#endif